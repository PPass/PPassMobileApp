import { RootLanguage } from '../../languageSettings/rootLanguage';
import { NativeModules } from 'react-native';
import { UserDataController } from '../securedStorage/controlers';
import { UserData } from '../screens/settings/types';

class GlobalState {
  userData: UserData = {
    language: 'en_US',
    userName: undefined,
    isRegistered: false,
    enhancedProtection: { active: false, additionalPassword: false },
  };
  languageHandler: RootLanguage;

  constructor() {
    this.languageHandler = new RootLanguage(this.userData.language);
  }

  initializeLanguageSettings = async (): Promise<void> => {
    const controller = new UserDataController();

    await controller.getUserData().then(async (data) => {
      if (data) {
        this.userData = data;
      } else {
        this.userData.language = NativeModules.I18nManager.localeIdentifier;
        await controller.addUserData({
          language: NativeModules.I18nManager.localeIdentifier,
          userName: this.userData.userName ?? undefined,
          isRegistered: this.userData.isRegistered ?? false,
          enhancedProtection: this.userData.enhancedProtection ?? false,
        });
      }
      this.languageHandler = new RootLanguage(this.userData.language);
    });
  };
}

export const globalState = new GlobalState();
