export interface UserData {
  userName: string | undefined;
  isRegistered: boolean;
  language: string;
  enhancedProtection: { active: boolean; additionalPassword: boolean };
}
