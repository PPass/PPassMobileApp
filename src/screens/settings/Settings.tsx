import * as React from 'react';
import { StyledButton, Text, View } from '../../styles/styledComponents/themed';
import { styles as styles } from './styles';
import { globalState } from '../../globalState/globalState';
import { handleFormatPasswords } from './handlers';
import { FontelloIcon } from '../../styles/styledComponents/icons';

export default function Settings(): JSX.Element {
  const userData = globalState.userData;

  return (
    <View style={styles.container}>
      <FontelloIcon style={styles.icon} name="adult" size={45} color="rgba(190, 190, 190, 1)" />
      <Text style={styles.title}>
        {globalState.languageHandler.accountResponses.WELCOME}
        {globalState.userData.userName ?? globalState.languageHandler.accountResponses.YOU_USER}
      </Text>
      <View style={styles.accountInfo}>
        <Text style={styles.row}>
          <Text>{globalState.languageHandler.accountResponses.REGISTERED_ON_SERVER}</Text>
          {userData.isRegistered ? (
            <FontelloIcon name="check" size={25} color="green" />
          ) : (
            <FontelloIcon name="block" size={25} color="red" />
          )}
        </Text>
        <Text style={styles.row}>
          <Text>{globalState.languageHandler.accountResponses.ENHANCED_PROTECTION}</Text>
          {userData.enhancedProtection ? (
            <FontelloIcon name="check" size={25} color="green" />
          ) : (
            <FontelloIcon name="block" size={25} color="red" />
          )}
        </Text>
      </View>
      <StyledButton style={styles.formatButton} onPress={async (): Promise<void> => handleFormatPasswords()}>
        <Text>{globalState.languageHandler.generic.FORMAT_DATA}</Text>
      </StyledButton>
    </View>
  );
}
