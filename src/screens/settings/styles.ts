import { StyleSheet } from 'react-native';
import { defaultProps } from '../../styles/defaultStyles/layout';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    width: 120,
    height: 120,
    textAlign: 'center',
    fontSize: 100,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 100,
    padding: 20,
  },
  title: {
    fontSize: defaultProps.fontSize,
    fontWeight: 'bold',
    width: defaultProps.window.width / 1.2,
    textAlign: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  accountInfo: {
    width: defaultProps.window.width / 1.2,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  row: {
    fontSize: defaultProps.fontSize - 2,
    marginTop: 5,
    marginBottom: 5,
  },
  formatButton: {
    backgroundColor: 'red',
  },
});
