import { PasswordController } from '../passwords/controllers';

export class Controllers {
  async formatPasswords(): Promise<void> {
    const controller = new PasswordController();

    await controller.formatAllPasswords();
  }
}
