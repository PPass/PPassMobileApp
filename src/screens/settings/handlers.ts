import { Controllers } from './controllers';
import { store } from '../../redux/store';
import { rootActions } from '../../redux/actions/rootActions';
import { globalState } from '../../globalState/globalState';

export const handleFormatPasswords = async (): Promise<void> => {
  const controller = new Controllers();

  try {
    await controller.formatPasswords();
    store.dispatch(rootActions.enablePopup(globalState.languageHandler.generic.FORMATTING_DATA_MESSAGE));
  } catch (err) {
    console.log(err);
  }
};
