import * as React from 'react';
import { Text, View } from '../../../styles/styledComponents/themed';
import { styles as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function Scanner(): JSX.Element {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{globalState.languageHandler.generic.NOT_YET_IMPLEMENTED}</Text>
    </View>
  );
}
