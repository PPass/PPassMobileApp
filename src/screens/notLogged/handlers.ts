import { store } from '../../redux/store';
import { rootActions } from '../../redux/actions/rootActions';
import { getRegisteredData, register } from './controllers';
import React from 'react';
import { globalState } from '../../globalState/globalState';
import CryptoES from 'crypto-es';

export const handlerLoginInput = async (password: string, setLogged: React.Dispatch<boolean>): Promise<void> => {
  const key = await getRegisteredData();
  const hashed = CryptoES.SHA512(password).toString();

  if (key) {
    if (key == hashed) return setLogged(true);
    store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.WRONG_PASSWORD));
  } else {
    store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.ACCOUNT_DOESNT_EXIST));
  }
};

export const handleRegisterInput = async (
  password: string,
  setScreen: React.Dispatch<'preRegister' | 'login' | 'register'>,
): Promise<void> => {
  if (!password) {
    store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.NO_PASSWORD));
    return;
  }
  if (password.length < 6) {
    store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.USER_PASSWORD_TOO_SHORT));
    return;
  }

  try {
    await register(password);
    setScreen('login');
  } catch (err) {
    console.log(err);
  }
};
