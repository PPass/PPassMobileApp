import * as React from 'react';
import { Dispatch } from 'react';
import { Scroll, StyledButton, StyledButtonText, Text } from '../../../styles/styledComponents/themed';
import { preRegisterScreen as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function Help(params: {
  setScreen: Dispatch<React.SetStateAction<'preRegister' | 'login' | 'register' | 'help'>>;
}): JSX.Element {
  return (
    <Scroll style={styles.helpContainer}>
      <Text style={styles.helpText}>{globalState.languageHandler.register.HELP_SCREEN}</Text>
      <StyledButton style={styles.helpButton} onPress={(): void => params.setScreen('register')}>
        <StyledButtonText>{globalState.languageHandler.register.REGISTER}</StyledButtonText>
      </StyledButton>
    </Scroll>
  );
}
