import * as React from 'react';
import { Dispatch } from 'react';
import { Pressable, TouchableOpacity } from 'react-native';
import { Text, View } from '../../../styles/styledComponents/themed';
import { preRegisterScreen as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';
import { FontelloIcon } from '../../../styles/styledComponents/icons';
import useColorScheme from '../../../hooks/useColorScheme';
import Colors from '../../../styles/defaultStyles/colors';

export default function PreRegister(params: {
  setScreen: Dispatch<React.SetStateAction<'preRegister' | 'login' | 'register' | 'help'>>;
}): JSX.Element {
  const colorScheme = useColorScheme();

  return (
    <View style={styles.preRegisterContainer}>
      <View>
        <Text style={styles.headerText}>{globalState.languageHandler.register.WELCOME}</Text>
      </View>
      <View style={styles.message}>
        <Text style={styles.headerWelcomeText}>{globalState.languageHandler.register.PRE_REGISTER_HEADER}</Text>
      </View>
      <View style={styles.bottomButtons}>
        <Pressable style={styles.button} onPress={(): void => params.setScreen('help')}>
          <Text style={styles.buttonText}>?</Text>
        </Pressable>
        <TouchableOpacity style={styles.touchableIcon} onPress={(): void => params.setScreen('register')}>
          <FontelloIcon name="right-thin" size={50} color={Colors[colorScheme].text} />
        </TouchableOpacity>
      </View>
    </View>
  );
}
