import * as React from 'react';
import { useState } from 'react';
import { Button } from 'react-native';
import { Input, Text, View } from '../../../styles/styledComponents/themed';
import { handlerLoginInput } from '../handlers';
import { LoginProps } from '../types';
import { loginScreen as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function Login({ ...props }: LoginProps): JSX.Element {
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{globalState.languageHandler.register.LOGIN}</Text>
      <View style={styles.flexVertical}>
        <Input
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={password}
          style={styles.passwordInput}
          onSubmitEditing={async (): Promise<void> => await handlerLoginInput(password, props.setIsLogged)}
          onChangeText={(data): void => setPassword(data)}
        />
        <Button
          onPress={async (): Promise<void> => await handlerLoginInput(password, props.setIsLogged)}
          title="->"
          color="orange"
          accessibilityLabel={globalState.languageHandler.register.LOGIN}
        />
      </View>
    </View>
  );
}
