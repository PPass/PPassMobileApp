import * as React from 'react';
import { useEffect, useState } from 'react';
import Login from './Login';
import { getRegisteredData } from '../controllers';
import { RootHandlerProps } from '../types';
import Register from './Register';
import PreRegister from './PreRegister';
import Help from './Help';

export function RootScreenHandler({ ...props }: RootHandlerProps): JSX.Element | null {
  const [screen, setScreen] = useState<'preRegister' | 'login' | 'register' | 'help'>('preRegister');
  const [isLoaded, setIsLoaded] = useState<boolean>();

  useEffect(() => {
    getRegisteredData().then((data) => {
      if (data && data.length > 0) {
        setScreen('login');
      }
      setIsLoaded(true);
    });
  }, []);

  const handleScreens = (): JSX.Element => {
    switch (screen) {
      case 'preRegister':
        return <PreRegister setScreen={setScreen} />;
      case 'register':
        return <Register setScreen={setScreen} />;
      case 'login':
        return <Login setIsLogged={props.setIsLogged} />;
      case 'help':
        return <Help setScreen={setScreen} />;
      default:
        return <PreRegister setScreen={setScreen} />;
    }
  };

  return isLoaded ? <>{handleScreens()}</> : null;
}
