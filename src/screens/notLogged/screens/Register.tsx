import * as React from 'react';
import { useState } from 'react';
import { Button } from 'react-native';
import { Input, Text, View } from '../../../styles/styledComponents/themed';
import { handleRegisterInput } from '../handlers';
import { RegisterProps } from '../types';
import { rootRegisterHandler as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function Register({ ...props }: RegisterProps): JSX.Element {
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{globalState.languageHandler.register.REGISTER}</Text>
      <View style={styles.flexVertical}>
        <Input
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={password}
          style={styles.passwordInput}
          onSubmitEditing={async (): Promise<void> => await handleRegisterInput(password, props.setScreen)}
          onChangeText={(data): void => setPassword(data)}
        />
        <Button
          onPress={async (): Promise<void> => await handleRegisterInput(password, props.setScreen)}
          title="->"
          color="orange"
          accessibilityLabel={globalState.languageHandler.register.REGISTER}
        />
      </View>
    </View>
  );
}
