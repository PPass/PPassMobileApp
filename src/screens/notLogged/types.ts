import React from 'react';

export interface LoginProps {
  setIsLogged: React.Dispatch<boolean>;
}

export interface RootHandlerProps {
  setIsLogged: React.Dispatch<boolean>;
}

export interface RegisterProps {
  setScreen: React.Dispatch<'preRegister' | 'login' | 'register'>;
}
