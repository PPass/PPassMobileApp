import { SecureStoreHandler } from '../../securedStorage/handlers';
import * as SecureStore from 'expo-secure-store';
import CryptoES from 'crypto-es';

export const register = async (password: string): Promise<void> => {
  const storage = new SecureStoreHandler();

  const hashed = CryptoES.SHA512(password).toString();
  await storage.addKey('isRegistered', hashed);
};

export const getRegisteredData = async (): Promise<string | null> => {
  const storage = new SecureStoreHandler();
  return await storage.getKey('isRegistered');
};

export const formatData = async (): Promise<void> => {
  await SecureStore.deleteItemAsync('userPassword');
  await SecureStore.deleteItemAsync('userData');
  await SecureStore.deleteItemAsync('isRegistered');
};
