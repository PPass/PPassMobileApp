import { StyleSheet } from 'react-native';
import { defaultProps } from '../../styles/defaultStyles/layout';

export const rootRegisterHandler = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  passwordInput: {
    marginTop: 15,
    marginRight: 10,
    marginLeft: 20,
    fontSize: defaultProps.fontSize,
    borderWidth: 2,
    borderTopColor: '#B0B0B000',
    borderRightColor: '#B0B0B000',
    borderLeftColor: '#B0B0B000',
    borderBottomColor: 'grey',
    borderTopWidth: 0,
    borderBottomWidth: 2,
    width: 180,
    textAlign: 'center',
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
  flexVertical: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
});

export const preRegisterScreen = StyleSheet.create({
  preRegisterContainer: {
    height: defaultProps.window.height,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  helpContainer: {
    height: defaultProps.window.height,
  },
  helpText: {
    marginTop: 60,
    marginLeft: 20,
    textAlign: 'left',
    fontSize: 21,
    fontWeight: 'bold',
    maxWidth: defaultProps.window.width * 0.7,
  },
  helpButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 200,
    height: 50,
    borderRadius: 30,
    backgroundColor: 'rgba(8, 114, 212, 0.40)',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  headerText: {
    marginTop: 5,
    marginLeft: 20,
    textAlign: 'left',
    fontSize: 30,
    fontWeight: 'bold',
  },
  touchableIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 100,
  },
  message: {
    marginLeft: 20,
    textAlign: 'left',
    fontSize: defaultProps.fontSize,
    maxWidth: 230,
  },
  bottomButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  headerWelcomeText: {
    fontSize: defaultProps.fontSize,
  },
  registerButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    borderRadius: 30,
    backgroundColor: 'rgba(8, 114, 212, 0.13)',
  },
  buttonText: {
    fontSize: 35,
    fontWeight: 'bold',
  },
});

export const loginScreen = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexVertical: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  passwordInput: {
    marginTop: 15,
    marginRight: 5,
    marginLeft: 20,
    fontSize: defaultProps.fontSize,
    borderWidth: 2,
    borderTopColor: '#B0B0B000',
    borderRightColor: '#B0B0B000',
    borderLeftColor: '#B0B0B000',
    borderBottomColor: 'grey',
    borderTopWidth: 0,
    borderBottomWidth: 2,
    width: 220,
    textAlign: 'center',
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
  break: {
    marginTop: 50,
  },
});
