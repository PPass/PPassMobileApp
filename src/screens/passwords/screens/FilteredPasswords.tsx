import * as React from 'react';
import { useEffect, useState } from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import { SemiTransparentInput, SemiTransparentView, View } from '../../../styles/styledComponents/themed';
import { FilterPassInterface } from '../types';
import { addPasswordStyles as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function FilteredPasswords({ ...props }: FilterPassInterface): JSX.Element {
  const [passwords, setPasswords] = useState<string[]>([]);
  const [inputData, setInputData] = useState<string>();

  useEffect(() => {
    setPasswords(props.passwords);
  });

  const handleFilter = (data: string): void => {
    setInputData(data);
    const filtered: string[] = [];

    for (let x = 0; x < passwords.length; x++) {
      if (passwords[x].toLowerCase().includes(data.toLowerCase())) {
        filtered.push(passwords[x]);
      }
    }
    props.setPasswords(filtered);
  };

  return (
    <Modal animationType="slide" transparent={true} visible={true} style={styles.flexData}>
      <TouchableWithoutFeedback onPress={(): void => props.disableScreen(false)}>
        <View style={styles.centeredView} />
      </TouchableWithoutFeedback>
      <SemiTransparentView style={styles.filterView}>
        <SemiTransparentInput
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={inputData}
          onChangeText={(data): void => handleFilter(data)}
        />
      </SemiTransparentView>
    </Modal>
  );
}
