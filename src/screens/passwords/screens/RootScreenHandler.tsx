import { TouchableOpacity } from 'react-native';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { Scroll, Text, View } from '../../../styles/styledComponents/themed';
import AddPassword from './AddPassword';
import { passwordElement as elementStyles, passwordsScreenHandler as screenStyles } from '../styles';
import { FontelloIcon } from '../../../styles/styledComponents/icons';
import EditPassword from './EditPassword';
import { handleFilterPasswords, handlerRemovePassword } from '../handlers';
import ViewPasswords from './ViewPasswords';
import FilterScreen from './FilteredPasswords';
import Confirmation from '../../generic/screens/Confirmation';
import { globalState } from '../../../globalState/globalState';
import { PasswordController } from '../controllers';

export function RootPasswordsHandler(): JSX.Element {
  const [add, setAdd] = useState<boolean>(false);
  const [edit, setEdit] = useState<boolean>(false);
  const [view, setView] = useState<boolean>(false);
  const [confirmation, setConfirmation] = useState<boolean>(false);
  const [filter, setFilter] = useState<boolean>(false);
  const [lockData, setLockData] = useState<React.Dispatch<boolean>>();

  const [passwords, setPasswords] = useState<string[]>([]);
  const [filteredPasswords, setFilteredPasswords] = useState<string[]>([]);
  const [passwordToEdit, setPasswordToEdit] = useState<string>('');

  useEffect(() => {
    const controller = new PasswordController();

    controller.getPasswords().then((data) => {
      if (data && data.length > 0) {
        setPasswords([...data]);
        setFilteredPasswords([...data]);
        handleRender();
      }
    });
  }, [passwords.length]);

  const handleRender = (): JSX.Element[] | JSX.Element => {
    if (filteredPasswords.length > 0) {
      return filteredPasswords.map((pass) => {
        return (
          <View key={pass} style={pass.length > 20 ? elementStyles.horizontalContainer : elementStyles.container}>
            <Text style={screenStyles.title}>{pass.replace('-_-_', ' ')}</Text>
            <View style={elementStyles.secondContainer}>
              <TouchableOpacity style={elementStyles.passwordViewButton}>
                <FontelloIcon
                  onPress={(): void => {
                    setLockData(() => setView);
                    setConfirmation(true);
                    setPasswordToEdit(pass);
                  }}
                  name="eye"
                  size={20}
                  style={{ padding: 8 }}
                  color="white"
                />
              </TouchableOpacity>
              <TouchableOpacity style={elementStyles.passwordDeleteButton}>
                <FontelloIcon
                  name="minus-circled"
                  size={35}
                  color="white"
                  onPress={async (): Promise<void> => await handlerRemovePassword(pass, setPasswords, passwords)}
                />
              </TouchableOpacity>
              <TouchableOpacity style={elementStyles.passwordEditButton}>
                <FontelloIcon
                  onPress={(): void => {
                    setPasswordToEdit(pass);
                    setEdit(true);
                  }}
                  name="pencil"
                  size={20}
                  style={{ padding: 8 }}
                  color="white"
                />
              </TouchableOpacity>
            </View>
          </View>
        );
      });
    } else {
      return (
        <Text
          style={{
            ...screenStyles.title,
            textAlign: 'center',
          }}
        >
          {globalState.languageHandler.errors.NO_PASSWORDS}
        </Text>
      );
    }
  };

  return (
    <View style={screenStyles.container}>
      <Scroll>
        {handleRender()}
        <View style={elementStyles.scrollBreaker} />
      </Scroll>
      <TouchableOpacity style={screenStyles.passwordAddButton}>
        <FontelloIcon onPress={(): void => setAdd(true)} name="plus-circled" size={55} color="#01a699" />
      </TouchableOpacity>
      <TouchableOpacity style={screenStyles.passwordSearchButton}>
        <FontelloIcon
          onPress={(): void => handleFilterPasswords(setFilteredPasswords, passwords, filteredPasswords, setFilter)}
          name="search"
          size={45}
          color="rgba(8, 114, 212, 0.70)"
        />
      </TouchableOpacity>
      {confirmation ? <Confirmation enableScreen={lockData} changeView={setConfirmation} /> : null}
      {filter ? (
        <FilterScreen
          disableScreen={setFilter}
          setPasswords={setFilteredPasswords}
          filteredPasswords={filteredPasswords}
          passwords={passwords}
        />
      ) : null}
      {add ? <AddPassword setAdd={setAdd} setPasswords={setPasswords} passwords={passwords} /> : null}
      {edit ? <EditPassword setEdit={setEdit} password={passwordToEdit} /> : null}
      {view ? <ViewPasswords setView={setView} password={passwordToEdit} /> : null}
    </View>
  );
}
