import * as React from 'react';
import { useEffect, useState } from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import {
  SemiTransparentView,
  StyledButton,
  StyledButtonText,
  Text,
  View,
} from '../../../styles/styledComponents/themed';
import * as Clipboard from 'expo-clipboard';
import { ViewPassInterface } from '../types';
import { addPasswordStyles as styles } from '../styles';
import { store } from '../../../redux/store';
import { rootActions } from '../../../redux/actions/rootActions';
import { globalState } from '../../../globalState/globalState';
import { PasswordController } from '../controllers';

export default function ViewPasswords({ ...props }: ViewPassInterface): JSX.Element {
  const [password, setPassword] = useState<string>('');

  useEffect(() => {
    const controller = new PasswordController();
    controller
      .getPassword(props.password)
      .then((data) => {
        if (data) setPassword(data);
      })
      .catch((err) => console.log(err));
  });

  return (
    <Modal animationType="slide" transparent={true} visible={true} style={styles.flexData}>
      <TouchableWithoutFeedback onPress={(): void => props.setView(false)}>
        <View style={styles.centeredView} />
      </TouchableWithoutFeedback>
      <SemiTransparentView style={styles.modalView}>
        <Text style={styles.passwordHeader}>{props.password.replace('-_-_', ' ')}</Text>
        <Text style={styles.passwordData}>{password}</Text>
        <StyledButton
          onPress={async (): Promise<void> => {
            try {
              await Clipboard.setString(`${password}`);
              store.dispatch(rootActions.enablePopup(globalState.languageHandler.generic.COPIED_TO_CLIPBOARD));
              props.setView(false);
            } catch (error) {
              console.log(error);
            }
          }}
          accessibilityLabel={globalState.languageHandler.generic.COPY_TO_CLIPBOARD}
        >
          <StyledButtonText>{globalState.languageHandler.generic.COPY}</StyledButtonText>
        </StyledButton>
      </SemiTransparentView>
    </Modal>
  );
}
