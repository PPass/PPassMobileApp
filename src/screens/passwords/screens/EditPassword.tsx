import * as React from 'react';
import { useState } from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import {
  SemiTransparentInput,
  SemiTransparentView,
  StyledButton,
  StyledButtonText,
  Text,
  View,
} from '../../../styles/styledComponents/themed';
import { EditPassInterface } from '../types';
import { handlerEditPassword } from '../handlers';
import { addPasswordStyles as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function EditPassword({ ...props }: EditPassInterface): JSX.Element {
  const [password, setPassword] = useState('');

  return (
    <Modal animationType="slide" transparent={true} visible={true} style={styles.flexData}>
      <TouchableWithoutFeedback onPress={(): void => props.setEdit(false)}>
        <View style={styles.centeredView} />
      </TouchableWithoutFeedback>
      <SemiTransparentView style={styles.modalView}>
        <Text style={styles.passwordHeader}>{props.password}</Text>
        <SemiTransparentInput
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={password}
          onSubmitEditing={async (): Promise<void> =>
            await handlerEditPassword(props.password, password, props.setEdit)
          }
          onChangeText={(data): void => setPassword(data)}
        />
        <StyledButton
          onPress={async (): Promise<void> => await handlerEditPassword(props.password, password, props.setEdit)}
        >
          <StyledButtonText>{globalState.languageHandler.register.EDIT}</StyledButtonText>
        </StyledButton>
      </SemiTransparentView>
    </Modal>
  );
}
