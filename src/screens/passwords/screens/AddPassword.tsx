import * as React from 'react';
import { useState } from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import {
  SemiTransparentInput,
  SemiTransparentView,
  StyledButton,
  StyledButtonText,
  Text,
  View,
} from '../../../styles/styledComponents/themed';
import { AddPassInterface } from '../types';
import { handlerAddPassword } from '../handlers';
import { addPasswordStyles as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export default function AddPassword({ ...props }: AddPassInterface): JSX.Element {
  const [password, setPassword] = useState('');
  const [passwordName, setPasswordName] = useState('');

  return (
    <Modal animationType="slide" transparent={true} visible={true} style={styles.flexData}>
      <TouchableWithoutFeedback onPress={(): void => props.setAdd(false)}>
        <View style={styles.centeredView} />
      </TouchableWithoutFeedback>
      <SemiTransparentView style={styles.modalView}>
        <Text style={styles.passwordHeader}>{globalState.languageHandler.register.ADD_PASSWORD}</Text>
        <SemiTransparentInput
          keyboardType="default"
          placeholder={globalState.languageHandler.register.SERVICE_NAME}
          value={passwordName}
          onChangeText={(data): void => setPasswordName(data)}
        />
        <SemiTransparentInput
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={password}
          onChangeText={(data): void => setPassword(data)}
        />
        <StyledButton
          onPress={async (): Promise<void> =>
            await handlerAddPassword(passwordName, password, props.setPasswords, props.passwords, props.setAdd)
          }
          accessibilityLabel={globalState.languageHandler.register.ADD_PASSWORD}
        >
          <StyledButtonText>{globalState.languageHandler.register.ADD}</StyledButtonText>
        </StyledButton>
      </SemiTransparentView>
    </Modal>
  );
}
