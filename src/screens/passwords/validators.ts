import { store } from '../../redux/store';
import { rootActions } from '../../redux/actions/rootActions';
import { globalState } from '../../globalState/globalState';

export const validatePassword = (key: string, password: string): void => {
  if (key.length < 3) {
    throw store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.PASSWORD_NAME_TOO_SHORT));
  }
  if (password.length < 6) {
    throw store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.PASSWORD_TOO_SHORT));
  }
};

export const alreadyExists = (password: string | undefined): void => {
  if (password) throw globalState.languageHandler.errors.PASSWORD_ALREADY_EXISTS;
};

export const isNameTooLong = (name: string): void => {
  if (name.length > 35) throw globalState.languageHandler.errors.PASSWORD_NAME_TOO_LONG;
};
