import React from 'react';
import * as valid from './validators';
import { store } from '../../redux/store';
import { rootActions } from '../../redux/actions/rootActions';
import { PasswordController } from './controllers';

export const handlerFormatPasswords = async (): Promise<void> => {
  const controller = new PasswordController();

  await controller.formatAllPasswords();
};

export const handlerAddPassword = async (
  key: string,
  password: string,
  setPasswords: React.Dispatch<string[]>,
  passwords: string[],
  setAdd: React.Dispatch<boolean>,
): Promise<void> => {
  const controller = new PasswordController();

  try {
    const existing = await controller.getPassword(key);
    valid.alreadyExists(existing);
    valid.isNameTooLong(key);

    await controller.addPassword(key, password);
    setPasswords([...passwords, key]);
    setAdd(false);
  } catch (err) {
    store.dispatch(rootActions.enablePopup(err));
    console.log(err);
  }
};

export const handlerEditPassword = async (
  key: string,
  password: string,
  setEdit: React.Dispatch<boolean>,
): Promise<void> => {
  const controller = new PasswordController();

  try {
    await controller.editPassword(key, password);
    setEdit(false);
  } catch (err) {
    console.log(err);
  }
};

export const handlerRemovePassword = async (
  key: string,
  setPasswords: React.Dispatch<string[]>,
  passwords: string[],
): Promise<void> => {
  const controller = new PasswordController();

  try {
    await controller.removePassword(key);
    setPasswords(passwords.filter((pass) => pass != key));
  } catch (err) {
    console.log(err);
  }
};

export const handleFilterPasswords = (
  setPasswords: React.Dispatch<string[]>,
  passwords: string[],
  filteredPasswords: string[],
  enableScreen: React.Dispatch<boolean>,
): void => {
  if (passwords.length != filteredPasswords.length) {
    setPasswords(passwords);
  } else {
    enableScreen(true);
  }
};
