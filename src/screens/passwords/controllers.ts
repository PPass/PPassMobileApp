import latinize from 'latinize';
import { validatePassword } from './validators';
import { SecureStoreHandler } from '../../securedStorage/handlers';

export class PasswordController {
  controller = new SecureStoreHandler();

  getPasswords = async (): Promise<string[]> => {
    const passwords = await this.controller.getKey('passwords');

    if (passwords) return JSON.parse(passwords) as string[];
    return [];
  };

  getPassword = async (password: string): Promise<string | undefined> => {
    const passwords = await this.controller.getKey(password);

    if (passwords) return passwords;
    return undefined;
  };

  async addPassword(key: string, password: string): Promise<void> {
    const filteredKey = latinize(key).replace(' ', '-_-_');
    validatePassword(filteredKey, password);

    const passwords = await this.controller.getKey('passwords');

    if (passwords && JSON.parse(passwords).length > 0) {
      const parsed: string[] = JSON.parse(passwords);
      parsed.push(filteredKey);
      await this.controller.addKey('passwords', JSON.stringify(parsed));
      return await this.controller.addKey(filteredKey, password);
    }
    await this.controller.addKey('passwords', JSON.stringify([filteredKey]));
    await this.controller.addKey(filteredKey, password);
  }

  async editPassword(key: string, password: string): Promise<void> {
    const passwords = await this.controller.getKey('passwords');
    const filtered = latinize(key).replace(' ', '-_-_');

    if (passwords) {
      const parsed: string[] = JSON.parse(passwords);
      if (!parsed.includes(filtered)) throw 'Password doesnt exists';
    } else {
      throw 'Password does not exists';
    }
    await this.controller.addKey(filtered, password);
  }

  async removePassword(key: string): Promise<void> {
    const passwords = await this.controller.getKey('passwords');

    if (passwords) {
      let parsed: string[] = JSON.parse(passwords);
      parsed = parsed.filter((password) => password != key);
      await this.controller.addKey('passwords', JSON.stringify(parsed));
    }

    await this.controller.removeKey(key);
  }

  formatAllPasswords = async (): Promise<void> => {
    const passwords = await this.controller.getKey('passwords');

    if (passwords) {
      const parsed: string[] = JSON.parse(passwords);
      for (const password of parsed) {
        await this.controller.removeKey(password);
      }
    }
    await this.controller.formatPasswords();
  };
}
