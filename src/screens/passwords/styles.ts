import { Dimensions, StyleSheet } from 'react-native';
import { defaultProps } from '../../styles/defaultStyles/layout';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const addPasswordStyles = StyleSheet.create({
  flexData: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  centeredView: {
    width: width,
    height: height,
    backgroundColor: undefined,
  },
  modalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingTop: 10,
    paddingBottom: 60,
    alignItems: 'center',
    width: width,
    height: height / 2,
  },
  filterView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    width: width,
    height: height / 4.5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  addNewPasswordInput: {
    marginTop: 15,
    marginRight: 5,
    marginLeft: 20,
    fontSize: 20,
    borderWidth: 2,
    backgroundColor: 'rgba(240, 240, 240, 0.8)',
    borderTopColor: '#B0B0B000',
    borderRightColor: '#B0B0B000',
    borderLeftColor: '#B0B0B000',
    borderBottomColor: 'grey',
    borderTopWidth: 0,
    borderBottomWidth: 2,
    width: 170,
    textAlign: 'center',
    fontWeight: 'bold',
    letterSpacing: 1.2,
    marginBottom: 10,
  },
  passwordHeader: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    fontSize: defaultProps.fontSize,
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
  passwordData: {
    fontSize: defaultProps.fontSize,
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
  passwordName: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    fontSize: 15,
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
});

export const passwordsScreenHandler = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  passwordAddButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 51,
    height: 51,
    position: 'absolute',
    bottom: 10,
    right: 10,
    borderRadius: 50,
  },
  passwordSearchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 51,
    height: 51,
    position: 'absolute',
    bottom: 80,
    right: 10,
    borderRadius: 50,
  },
});

const passwordButton = StyleSheet.create({
  passwordButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    marginLeft: 15,
    borderRadius: 100,
  },
});

export const passwordElement = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: width,
    minHeight: 70,
    paddingLeft: 5,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#c9c9c9',
  },
  horizontalContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: width,
    minHeight: 70,
    paddingLeft: 5,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#c9c9c9',
  },
  scrollBreaker: {
    width: width,
    height: 70,
  },
  secondContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: 5,
  },
  passwordDeleteButton: {
    ...passwordButton.passwordButton,
    backgroundColor: 'red',
    marginBottom: 1,
    paddingRight: 0.5,
  },
  passwordEditButton: {
    ...passwordButton.passwordButton,
    backgroundColor: 'rgba(8, 114, 212, 0.70)',
  },
  passwordViewButton: {
    ...passwordButton.passwordButton,
    backgroundColor: '#01a699',
  },
});
