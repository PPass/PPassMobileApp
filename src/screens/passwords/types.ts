import React from 'react';

export interface HomeInterface {
  setVisible: React.Dispatch<boolean>;
  passwords?: string[];
}

export interface AddPassInterface {
  setAdd: React.Dispatch<boolean>;
  setPasswords: React.Dispatch<string[]>;
  passwords: string[];
}

export interface EditPassInterface {
  setEdit: React.Dispatch<boolean>;
  password: string;
}

export interface ViewPassInterface {
  setView: React.Dispatch<boolean>;
  password: string;
}

export interface FilterPassInterface {
  disableScreen: React.Dispatch<boolean>;
  setPasswords: React.Dispatch<string[]>;
  passwords: string[];
  filteredPasswords: string[];
}

export interface ConfirmationInterface {
  changeView: React.Dispatch<boolean>;
  enableScreen: React.Dispatch<boolean> | undefined;
}
