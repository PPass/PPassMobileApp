import * as React from 'react';
import { Scroll, Text, View } from '../../../styles/styledComponents/themed';
import { styles } from '../styles';
import { globalState } from '../../../globalState/globalState';

export function Help(): JSX.Element {
  return (
    <View style={styles.modalBody}>
      <Text style={styles.header}>FAQ</Text>
      <Scroll style={styles.questions}>
        <Text style={styles.question}>1. {globalState.languageHandler.helpMessages.WHY_NOT_CONNECTED}</Text>
        <Text style={styles.answer}>{globalState.languageHandler.helpMessages.WHY_NOT_CONNECTED_ANSWER}</Text>
        <Text style={styles.question}>2. {globalState.languageHandler.helpMessages.WHY_UNDER_CONSTRUCTION}</Text>
        <Text style={styles.answer}>{globalState.languageHandler.helpMessages.WHY_UNDER_CONSTRUCTION_ANSWER}</Text>
        <Text style={styles.question}>3. {globalState.languageHandler.helpMessages.SECURED}</Text>
        <Text style={styles.answer}>{globalState.languageHandler.helpMessages.SECURED_ANSWER}</Text>
      </Scroll>
    </View>
  );
}
