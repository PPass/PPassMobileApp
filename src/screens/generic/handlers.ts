import { getLoginData } from '../notLogged/controllers';
import { store } from '../../redux/store';
import { rootActions } from '../../redux/actions/rootActions';
import { globalState } from '../../globalState/globalState';

export const handleCheckLogin = async (password: string): Promise<boolean> => {
  const key = await getLoginData();
  if (key) {
    if (password == key) return true;

    store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.WRONG_PASSWORD));
  }
  store.dispatch(rootActions.enablePopup(globalState.languageHandler.errors.NO_PASSWORD));
  return false;
};
