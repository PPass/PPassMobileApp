import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Text, View } from '../../../styles/styledComponents/themed';

import { RootStackScreenProps } from '../../../../types';
import { globalState } from '../../../globalState/globalState';

export default function NotFound({ navigation }: RootStackScreenProps<'NotFound'>): JSX.Element {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{globalState.languageHandler.errors.SCREEN_DOES_NOT_EXIST}</Text>
      <TouchableOpacity onPress={(): void => navigation.replace('Root')} style={styles.link}>
        <Text style={styles.linkText}>{globalState.languageHandler.generic.GO_HOME}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  link: {
    marginTop: 15,
    paddingVertical: 15,
  },
  linkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
