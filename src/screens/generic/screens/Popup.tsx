import * as React from 'react';
import { useEffect, useState } from 'react';
import { StyledButton, StyledButtonText, StyledModal, Text, View } from '../../../styles/styledComponents/themed';

import { store } from '../../../redux/store';
import { rootActions } from '../../../redux/actions/rootActions';
import { styles as styles } from '../styles';
import { globalState } from '../../../globalState/globalState';
import { PopupAction } from '../../../redux/interfaces/actionsInterface';

export default function Popup(): JSX.Element {
  const popupData = store.getState().popupReducer.message;

  const [visible, setVisible] = useState(false);

  const togglePopup = (): void => {
    const popupStatus = store.getState().popupReducer.enabled;
    setVisible(popupStatus);
  };

  useEffect(() => {
    const unsubscribe = store.subscribe(togglePopup);
    return (): void => {
      unsubscribe();
    };
  }, []);

  return (
    <StyledModal animationType="fade" transparent={true} visible={visible}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>{popupData}</Text>
          <StyledButton onPress={(): PopupAction => store.dispatch(rootActions.disablePopup())}>
            <StyledButtonText style={styles.textStyle}>{globalState.languageHandler.generic.GOT_IT}</StyledButtonText>
          </StyledButton>
        </View>
      </View>
    </StyledModal>
  );
}
