import * as React from 'react';
import { useState } from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import {
  SemiTransparentInput,
  SemiTransparentView,
  StyledButton,
  StyledButtonText,
  Text,
  View,
} from '../../../styles/styledComponents/themed';
import { addPasswordStyles as styles } from '../../passwords/styles';
import { ConfirmationInterface } from '../../passwords/types';
import { handleCheckLogin } from '../handlers';
import { globalState } from '../../../globalState/globalState';

export default function Confirmation({ ...props }: ConfirmationInterface): JSX.Element {
  const [password, setPassword] = useState('');

  return (
    <Modal animationType="slide" transparent={true} visible={true} style={styles.flexData}>
      <TouchableWithoutFeedback onPress={(): void => props.changeView(false)}>
        <View style={styles.centeredView} />
      </TouchableWithoutFeedback>
      <SemiTransparentView style={styles.modalView}>
        <Text style={styles.passwordHeader}>{globalState.languageHandler.register.CONFIRM_MESSAGE}</Text>
        <SemiTransparentInput
          secureTextEntry={true}
          placeholder={globalState.languageHandler.register.PASSWORD}
          value={password}
          onSubmitEditing={async (): Promise<void> => {
            handleCheckLogin(password).then((data) => {
              if (data) {
                props.changeView(false);
                if (props.enableScreen) props.enableScreen(true);
              }
            });
          }}
          onChangeText={(data): void => setPassword(data)}
        />
        <StyledButton
          onPress={async (): Promise<void> => {
            handleCheckLogin(password).then((data) => {
              if (data) {
                props.changeView(false);
                if (props.enableScreen) props.enableScreen(true);
              }
            });
          }}
          accessibilityLabel={globalState.languageHandler.register.CONFIRM}
        >
          <StyledButtonText>{globalState.languageHandler.register.CONFIRM}</StyledButtonText>
        </StyledButton>
      </SemiTransparentView>
    </Modal>
  );
}
