import { StyleSheet } from 'react-native';
import { defaultProps } from '../../styles/defaultStyles/layout';

export const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    fontSize: 17,
    fontWeight: 'bold',
    letterSpacing: 0.6,
    marginBottom: 15,
    textAlign: 'center',
  },
  modalBody: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  header: {
    fontSize: defaultProps.fontSize + 5,
  },
  questions: {
    width: defaultProps.window.width - defaultProps.window.width * 0.1,
  },
  question: {
    fontSize: defaultProps.fontSize,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  answer: {
    fontSize: defaultProps.fontSize - 3,
    marginBottom: 20,
  },
});
