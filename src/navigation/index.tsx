/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DarkTheme, DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Pressable } from 'react-native';

import Colors from '../styles/defaultStyles/colors';
import useColorScheme from '../hooks/useColorScheme';
import NotFound from '../screens/generic/screens/NotFound';
import Settings from '../screens/settings/Settings';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../../types';
import LinkingConfiguration from './LinkingConfiguration';
import Scanner from '../screens/scanner/screens/Scanner';
import { RootPasswordsHandler } from '../screens/passwords/screens/RootScreenHandler';
import { globalState } from '../globalState/globalState';
import { Help } from '../screens/generic/modals/Help';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }): JSX.Element {
  return (
    <NavigationContainer linking={LinkingConfiguration} theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator(): JSX.Element {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFound} options={{ title: 'Oops!' }} />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen name="SettingsModal" component={Help} />
      </Stack.Group>
    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator(): JSX.Element {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors[colorScheme].background,
        },
        tabBarStyle: {
          backgroundColor: Colors[colorScheme].background,
        },

        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={RootPasswordsHandler}
        options={(): {
          tabBarIcon: ({ color }: { color: string }) => JSX.Element;
          title: string;
        } => ({
          title: globalState.languageHandler.screenNames.HOME_SCREEN,
          tabBarIcon: ({ color }): JSX.Element => <TabBarIcon name="home" color={color} />,
        })}
      />
      <BottomTab.Screen
        name="Scanner"
        component={Scanner}
        options={{
          title: globalState.languageHandler.screenNames.SCANNER,
          tabBarIcon: ({ color }): JSX.Element => <TabBarIcon name="camera" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Settings"
        component={Settings}
        options={({
          navigation,
        }: RootTabScreenProps<'Settings'>): {
          tabBarIcon: ({ color }: { color: string }) => JSX.Element;
          headerRight: () => JSX.Element;
          title: string;
        } => ({
          title: globalState.languageHandler.screenNames.SETTINGS,
          tabBarIcon: ({ color }): JSX.Element => <TabBarIcon name="gear" color={color} />,
          headerRight: (): JSX.Element => (
            <Pressable
              onPress={(): void => navigation.navigate('SettingsModal')}
              style={({ pressed }): { opacity: number } => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <FontAwesome name="question" size={25} color={Colors[colorScheme].text} style={{ marginRight: 15 }} />
            </Pressable>
          ),
        })}
      />
    </BottomTab.Navigator>
  );
}

function TabBarIcon(props: { name: React.ComponentProps<typeof FontAwesome>['name']; color: string }): JSX.Element {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}
