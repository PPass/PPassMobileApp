/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';

import { RootStackParamList } from '../../types';

const linking: LinkingOptions<RootStackParamList> = {
  prefixes: [Linking.createURL('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Home: {
            screens: {
              RootPasswords: {
                screens: {
                  AddPassword: 'addPassword',
                  EditPassword: 'editPassword',
                },
              },
              Scanner: {},
              Settings: {},
            },
          },
          RootRegister: {
            screens: {
              Register: 'register',
              PreRegister: 'preregister',
              Login: 'login',
            },
          },
        },
      },
      Modal: 'modal',
      Popup: 'popup',
      NotFound: '*',
    },
  },
};

export default linking;
