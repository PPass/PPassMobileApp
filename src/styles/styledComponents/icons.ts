import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './config/iconsConfig.json';

export const FontelloIcon = createIconSetFromFontello(fontelloConfig);
