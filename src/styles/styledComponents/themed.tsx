import * as React from 'react';
import {
  Modal,
  Pressable,
  ScrollView,
  StatusBar,
  StatusBarStyle,
  Text as DefaultText,
  TextInput,
  TouchableOpacity,
  View as DefaultView,
} from 'react-native';

import Colors, { barStyleColors } from '../defaultStyles/colors';
import useColorScheme from '../../hooks/useColorScheme';
import * as styles from './styles';

export function useThemeColor(
  props: { light?: string; dark?: string },
  colorName: keyof typeof Colors.light & keyof typeof Colors.dark,
): string {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

export function themePicker(props: { light?: string; dark?: string }): StatusBarStyle {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) return barStyleColors[theme] as StatusBarStyle;
  return 'default' as StatusBarStyle;
}

type ThemeProps = {
  lightColor?: string;
  darkColor?: string;
};

type TextProps = ThemeProps & DefaultText['props'];
type ViewProps = ThemeProps & DefaultView['props'];
type TextInputProps = ThemeProps & TextInput['props'];
type SemiTransparentProps = ThemeProps & DefaultView['props'];
type ModalProps = ThemeProps & Modal['props'];
type PressableProps = ThemeProps & TouchableOpacity['props'];

export function Text(props: TextProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function View(props: ViewProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function Input(props: TextInputProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <TextInput style={[{ backgroundColor, color }, style]} placeholderTextColor={color} {...otherProps} />;
}

export function SemiTransparentInput(props: TextInputProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return (
    <TextInput
      style={[{ color, ...styles.addPasswordStyles.semiTransparentInputStyle }, style]}
      placeholderTextColor={color}
      {...otherProps}
    />
  );
}

export function SemiTransparentView(props: SemiTransparentProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'semiTransparentBackground');

  return <View style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function Scroll(props: ViewProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <ScrollView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function StyledModal(props: ModalProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <Modal style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function StyledButton(props: PressableProps): JSX.Element {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'Button');

  return <Pressable style={[{ ...styles.addPasswordStyles.pressableStyle, backgroundColor }, style]} {...otherProps} />;
}

export function StyledButtonText(props: TextProps): JSX.Element {
  return <Text {...props} style={[props.style, { ...styles.addPasswordStyles.pressableButtonStyle }]} />;
}

export function ThemedStatusBar(props: ViewProps): JSX.Element {
  const { lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');
  const barStyle = themePicker({ light: 'dark-content', dark: 'light-content' });

  return <StatusBar backgroundColor={backgroundColor} barStyle={barStyle} {...otherProps} />;
}
