import { StyleSheet } from 'react-native';
import { defaultProps } from '../defaultStyles/layout';

export const addPasswordStyles = StyleSheet.create({
  pressableStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 200,
    height: 50,
    borderRadius: 30,
  },
  pressableButtonStyle: {
    fontFamily: 'space-mono',
    fontSize: 17,
    fontWeight: 'bold',
  },
  semiTransparentInputStyle: {
    marginTop: 15,
    marginRight: 5,
    marginLeft: 20,
    paddingTop: 15,
    fontSize: defaultProps.fontSize,
    borderWidth: 2,
    borderTopColor: '#B0B0B000',
    borderRightColor: '#B0B0B000',
    borderLeftColor: '#B0B0B000',
    borderBottomColor: 'grey',
    borderTopWidth: 0,
    borderBottomWidth: 2,
    width: 200,
    height: 50,
    textAlign: 'center',
    fontWeight: 'bold',
    letterSpacing: 1.2,
    marginBottom: 10,
  },
});
