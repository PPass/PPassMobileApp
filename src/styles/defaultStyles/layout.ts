import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const defaultProps = {
  window: {
    width,
    height,
  },
  fontSize: 22,
  isSmallDevice: width < 375,
};
