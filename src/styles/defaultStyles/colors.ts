const tintColorLight = '#2f95dc';
const lightBackground = '#fafafa';
const lightButton = 'rgba(8, 114, 212, 0.40)';
const lightText = '#1A1A1A';
const semiTransparentBackgroundLight = 'rgba(240, 240, 240, 0.9)';

// Good looking color
// rgba(8, 114, 212, 0.40)

const tintColorDark = '#fff';
const darkBackground = '#121212';
const darkButton = '#292929';
const darkText = '#FFFFFF';
const semiTransparentBackgroundDark = 'rgba(25, 25, 25, 0.9)';

export default {
  light: {
    text: lightText,
    semiTransparentBackground: semiTransparentBackgroundLight,
    background: lightBackground,
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    Button: lightButton,
  },
  dark: {
    text: darkText,
    semiTransparentBackground: semiTransparentBackgroundDark,
    background: darkBackground,
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    Button: darkButton,
  },
};

export const barStyleColors = {
  light: 'dark-content',
  dark: 'light-content',
};
