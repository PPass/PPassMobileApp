// Load fonts
import * as Font from 'expo-font';
import { FontAwesome } from '@expo/vector-icons';

export const loadFonts = async (): Promise<void> =>
  await Font.loadAsync({
    ...FontAwesome.font,
    fontello: require('../../assets/fonts/fontello.ttf'),
    'space-mono': require('../../assets/fonts/SpaceMono-Regular.ttf'),
  });
