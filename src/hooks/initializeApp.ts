import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';
import { loadFonts } from './loadFonts';
import { globalState } from '../globalState/globalState';

export default function initializeApp(): boolean {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  React.useEffect((): void => {
    loadResourcesAndDataAsync().then();
  }, []);

  async function loadResourcesAndDataAsync(): Promise<void> {
    try {
      await SplashScreen.preventAutoHideAsync();
      await loadFonts();
      await globalState.initializeLanguageSettings();
    } catch (e) {
      console.warn(e);
    } finally {
      setLoadingComplete(true);
      await SplashScreen.hideAsync();
    }
  }

  return isLoadingComplete;
}
