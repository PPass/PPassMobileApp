// Reducer
import { PopupAction, PopupState } from '../interfaces/actionsInterface';

export const popupReducer = (state: PopupState = { enabled: false, message: ' ' }, action: PopupAction): PopupState => {
  switch (action.type) {
    case 'ENABLE_POPUP':
      return (state = {
        enabled: true,
        message: action.message,
      });
    case 'DISABLE_POPUP':
    default:
      return (state = {
        enabled: false,
        message: '',
      });
  }
};
