import { PopupAction } from '../interfaces/actionsInterface';

export const enablePopup = (message: string): PopupAction => {
  return {
    type: 'ENABLE_POPUP',
    message: message,
  };
};

export const disablePopup = (): PopupAction => {
  return {
    type: 'DISABLE_POPUP',
    message: '',
  };
};
