// Account
import { disablePopup, enablePopup } from './popupActions';

export const rootActions = {
  enablePopup,
  disablePopup,
};
