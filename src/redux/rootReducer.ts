import { combineReducers } from 'redux';
import { popupReducer } from './reducers/popupReducer';

export default combineReducers({
  popupReducer,
});
