export interface PopupAction {
  type: string;
  message: string;
}

export interface PopupState {
  enabled: boolean;
  message: string;
}
