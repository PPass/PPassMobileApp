import * as SecureStore from 'expo-secure-store';

export class SecureStoreHandler {
  getKey = async (key: string): Promise<string | null> => {
    return await SecureStore.getItemAsync(key);
  };

  addKey = async (key: string, value: string): Promise<void> => {
    await SecureStore.setItemAsync(key, value);
  };

  removeKey = async (key: string): Promise<void> => {
    await SecureStore.deleteItemAsync(key);
  };

  formatPasswords = async (): Promise<void> => {
    await SecureStore.deleteItemAsync('passwords');
  };
}
