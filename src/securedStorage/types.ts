export interface PasswordDBInterface {
  [key: string]: string;
}
