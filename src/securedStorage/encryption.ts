import CryptoES from 'crypto-es';

export const generateMasterKey = (): string => {
  return CryptoES.lib.WordArray.random(512 / 4).toString();
};

export const encrypt = (password: string, key: string): string => {
  return CryptoES.AES.encrypt(password, key).toString();
};

export const decrypt = (password: string, key: string): string => {
  return CryptoES.AES.decrypt(password, key).toString(CryptoES.enc.Utf8);
};
