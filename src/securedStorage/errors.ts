export class DataDoesNotExist extends Error {
  constructor(type: string) {
    super('DataDoesNotExist');
    this.message = `Value ${type} does not exist in database`;
    this.name = 'DataDoesNotExist';
  }
}
