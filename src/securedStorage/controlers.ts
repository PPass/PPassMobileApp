import { SecureStoreHandler } from './handlers';
import { UserData } from '../screens/settings/types';

export class UserDataController {
  getUserData = async (): Promise<UserData | null> => {
    const storage = new SecureStoreHandler();

    return storage.getKey('userData').then((data) => {
      if (data) {
        return JSON.parse(data);
      } else {
        return null;
      }
    });
  };

  addUserData = async (data: UserData): Promise<void> => {
    const storage = new SecureStoreHandler();
    await storage.addKey('userData', JSON.stringify(data));
  };
}
