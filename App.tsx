import React, { useState } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import initializeApp from './src/hooks/initializeApp';
import useColorScheme from './src/hooks/useColorScheme';
import Navigation from './src/navigation';
import Popup from './src/screens/generic/screens/Popup';
import { RootScreenHandler } from './src/screens/notLogged/screens/RootScreenHandler';
import { StatusBar } from 'react-native';
import { ThemedStatusBar } from './src/styles/styledComponents/themed';

export default function App(): JSX.Element | null {
  const isLoadingComplete = initializeApp();
  const colorScheme = useColorScheme();
  const [isLogged, setIsLogged] = useState(false);

  if (!isLoadingComplete) {
    return null;
  } else {
    if (!isLogged) {
      return (
        <SafeAreaProvider>
          <ThemedStatusBar />
          <Popup />
          <RootScreenHandler setIsLogged={setIsLogged} />
        </SafeAreaProvider>
      );
    } else {
      return (
        <SafeAreaProvider>
          <ThemedStatusBar />
          <Popup />
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
        </SafeAreaProvider>
      );
    }
  }
}
