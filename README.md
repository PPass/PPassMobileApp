# PPass

### This app is simple password manager. It allows user to add/edit/remove your passwords

# 1. How to start

## Make sure you have java installed and other dependencies

## Install dependencies

### - "npm install" / "yarn install"

## Start project

### "npm start" \ "yarn start" will start web apps to command your app. You can also specify device you want to run. For that, check 2.

# 2. Useful commands

## "yarn android" / "npm run android" will start app on android device / emulator

## "yarn ios" / "npm run ios" will start ios app ( not tested by me. Don't have anything to test )

## "yarn assemble" / "npm run assemble" = assemble release apk's

## "yarn cleanAssemble" / "npm run cleanAssemble" = clear cache and assemble release apk's

## "yarn bundleStore" / "npm run bundleStore" = create .aab file for Google Play Store = required google play key called "gplay.keystore" inside /android/app

## "yarn linkAssests" / "npm run linkAssests" = links additional assets

## "yarn clean" / "npm run clean" = clear cache
