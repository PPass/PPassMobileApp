export enum GenericResponsesPL {
  GO_HOME = 'Przejdź do strony głównej',
  GOT_IT = 'Ok',
  COPIED_TO_CLIPBOARD = 'Skopiowano do schowka',
  COPY_TO_CLIPBOARD = 'Skopiuj do schowka',
  COPY = 'Kopiuj',
  NOT_YET_IMPLEMENTED = 'Ten element jest w trakcie budowy',
  FORMATTING_DATA_MESSAGE = 'Po zformatowaniu haseł, wyłącz i włącz aplikację w celu poprawnego skasowania danych',
  FORMAT_PASSWORDS = 'Nacisnij aby wyczyścić hasła',
  FORMATTING_APP_MESSAGE = 'Dane zostały wyczyszcone, wyłącz i włącz aplikację w celu poprawnego skasowania danych',
  FORMAT_DATA = 'Wyczyść dane aplikacji',
  FIND_PASSWORD = 'Szukaj hasła',
  EXIT = 'Wyjdź',
}
