import { GenericResponses } from './genericResponses';
import { GenericResponsesPL } from './genericResponsesPL';
import { GenericResponsesInterface } from '../types/genericResponsesInterface';

export const genericMessages = (userLanguage: string): GenericResponsesInterface => {
  switch (userLanguage) {
    case 'pl_PL':
      return GenericResponsesPL;
    case 'en_US':
      return GenericResponses;
    default:
      return GenericResponses;
  }
};
