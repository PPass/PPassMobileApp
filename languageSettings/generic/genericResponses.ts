export enum GenericResponses {
  GO_HOME = 'Go to home screen',
  GOT_IT = 'Got it',
  COPIED_TO_CLIPBOARD = 'Password copied to clipboard',
  COPY_TO_CLIPBOARD = 'Copy to clipboard',
  COPY = 'Copy',
  NOT_YET_IMPLEMENTED = 'Not yet implemented',
  FORMATTING_DATA_MESSAGE = 'After formatting passwords, make sure to reload this app ( close and open ), in order to finish the formatting process',
  FORMAT_PASSWORDS = 'Click to format passwords',
  FORMATTING_APP_MESSAGE = 'After formatting data, make sure to reload this app ( close and open ), in order to finish the formatting process',
  FORMAT_DATA = 'Format app',
  FIND_PASSWORD = 'Find password',
  EXIT = 'Exit',
}
