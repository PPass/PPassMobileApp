import { ScreenNames } from './screenNames';
import { ScreenNamesPL } from './screenNamesPL';
import { ScreenNamesInterface } from '../types/screenNamesInterface';

export const screenNames = (userLanguage: string): ScreenNamesInterface => {
  switch (userLanguage) {
    case 'pl_PL':
      return ScreenNamesPL;
    case 'en_US':
      return ScreenNames;
    default:
      return ScreenNames;
  }
};
