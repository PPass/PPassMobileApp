export enum ScreenNames {
  HOME_SCREEN = 'Passwords',
  SCANNER = 'Scanner',
  SETTINGS = 'Settings',
}
