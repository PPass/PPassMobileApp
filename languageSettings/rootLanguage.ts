import { errMessages } from './errorMessages/languageHandler';
import { genericMessages } from './generic/languageHandler';
import { registerMessages } from './newAccount/languageHandler';
import { ErrMessagesInterface } from './types/errMessagesInterface';
import { RegisterMessagesInterface } from './types/registerMessagesInterface';
import { GenericResponsesInterface } from './types/genericResponsesInterface';
import { screenNames } from './screens/languageHandler';
import { ScreenNamesInterface } from './types/screenNamesInterface';
import { accountResponses } from './accountResponses/languageHandler';
import { AccountResponsesInterface } from './types/accountResponsesInterface';
import { helpMessages } from './help/languageHandler';
import { HelpMessages } from './types/helpMessagesInterface';

export class RootLanguage {
  accountResponses: AccountResponsesInterface;
  errors: ErrMessagesInterface;
  generic: GenericResponsesInterface;
  register: RegisterMessagesInterface;
  screenNames: ScreenNamesInterface;
  helpMessages: HelpMessages;

  constructor(userLanguage: string) {
    this.accountResponses = accountResponses(userLanguage);
    this.errors = errMessages(userLanguage);
    this.generic = genericMessages(userLanguage);
    this.register = registerMessages(userLanguage);
    this.screenNames = screenNames(userLanguage);
    this.helpMessages = helpMessages(userLanguage);
  }
}
