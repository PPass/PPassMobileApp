export interface ErrMessagesInterface {
  WRONG_PASSWORD: string;
  NO_PASSWORD: string;
  NO_PASSWORDS: string;
  USER_PASSWORD_TOO_SHORT: string;
  PASSWORD_ALREADY_EXISTS: string;
  NOT_FOUND: string;
  SCREEN_DOES_NOT_EXIST: string;
  ACCOUNT_DOESNT_EXIST: string;
  PASSWORD_TOO_SHORT: string;
  PASSWORD_NAME_TOO_SHORT: string;
  PASSWORD_NAME_TOO_LONG: string;
}
