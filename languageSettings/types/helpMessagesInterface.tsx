export interface HelpMessages {
  WHY_NOT_CONNECTED: string;
  WHY_NOT_CONNECTED_ANSWER: string;
  WHY_UNDER_CONSTRUCTION: string;
  WHY_UNDER_CONSTRUCTION_ANSWER: string;
  MONETIZATION: string;
  MONETIZATION_ANSWER: string;
  TRANSLATION: string;
  TRANSLATION_ANSWER: string;
  SECURED: string;
  SECURED_ANSWER: string;
}
