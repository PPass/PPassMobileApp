export interface GenericResponsesInterface {
  GO_HOME: string;
  GOT_IT: string;
  COPIED_TO_CLIPBOARD: string;
  COPY_TO_CLIPBOARD: string;
  COPY: string;
  NOT_YET_IMPLEMENTED: string;
  FORMATTING_DATA_MESSAGE: string;
  FORMATTING_APP_MESSAGE: string;
  FORMAT_PASSWORDS: string;
  FORMAT_DATA: string;
  FIND_PASSWORD: string;
  EXIT: string;
}
