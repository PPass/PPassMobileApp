export interface AccountResponsesInterface {
  WELCOME: string;
  YOU_USER: string;
  NOT_REGISTERED: string;
  REGISTERED_INFORMATION: string;
  REGISTERED_ON_SERVER: string;
  ENHANCED_PROTECTION: string;
}
