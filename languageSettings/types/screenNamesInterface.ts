export interface ScreenNamesInterface {
  HOME_SCREEN: string;
  SCANNER: string;
  SETTINGS: string;
}
