export interface RegisterMessagesInterface {
  REGISTER: string;
  LOGIN: string;
  PASSWORD: string;
  CONFIRM_MESSAGE: string;
  CONFIRM: string;
  ADD_PASSWORD: string;
  SERVICE_NAME: string;
  ADD: string;
  EDIT: string;
  PRE_REGISTER_HEADER: string;
  WELCOME: string;
  HELP_SCREEN: string;
}
