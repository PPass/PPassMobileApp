export enum ErrorMessages {
  WRONG_PASSWORD = 'Wrong password.',
  NO_PASSWORD = 'Password not provided.',
  NO_PASSWORDS = 'You have 0 saved passwords.',
  USER_PASSWORD_TOO_SHORT = 'Good password should contain at least 6 characters.',
  PASSWORD_ALREADY_EXISTS = 'Password with that name already exists. Try another name.',
  NOT_FOUND = 'Not found.',
  SCREEN_DOES_NOT_EXIST = 'This screen does not exist.',
  ACCOUNT_DOESNT_EXIST = 'This account does not exist.',
  PASSWORD_TOO_SHORT = 'Good password should contain at least 8 characters.',
  PASSWORD_NAME_TOO_SHORT = 'Name should contain at least 3 characters',
  PASSWORD_NAME_TOO_LONG = 'Name should not be longer than 35 characters.',
}
