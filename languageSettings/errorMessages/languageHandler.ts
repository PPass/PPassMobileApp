import { ErrorMessages } from './errorMessages';
import { ErrorMessagesPl } from './errorMessagesPL';
import { ErrMessagesInterface } from '../types/errMessagesInterface';

export const errMessages = (userLanguage: string): ErrMessagesInterface => {
  switch (userLanguage) {
    case 'pl_PL':
      return ErrorMessagesPl;
    case 'en_US':
      return ErrorMessages;
    default:
      return ErrorMessages;
  }
};
