export enum ErrorMessagesPl {
  WRONG_PASSWORD = 'Błędne hasło.',
  NO_PASSWORD = 'Hasło nie zostało podane.',
  NO_PASSWORDS = 'Posiadasz 0 zapisanych haseł.',
  USER_PASSWORD_TOO_SHORT = 'Hasło powinno posiadać co najmniej 6 znaków w celu bezpieczeństwa.',
  PASSWORD_ALREADY_EXISTS = 'Podane nazwa hasła już istnieje. Spróbuj inną nazwę.',
  NOT_FOUND = 'Nie znaleziono.',
  SCREEN_DOES_NOT_EXIST = 'Ten ekran nie istnieje.',
  ACCOUNT_DOESNT_EXIST = 'Konto nie istnieje.',
  PASSWORD_TOO_SHORT = 'Dobre hasło powinno posiadać co najmniej 6 znaków.',
  PASSWORD_NAME_TOO_SHORT = 'Nazwa musi posiadać co najmniej 3 znaki.',
  PASSWORD_NAME_TOO_LONG = 'Nazwa hasła powinna zawierać do 35 znaków.',
}
