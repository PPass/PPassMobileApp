export enum AccountResponsesPl {
  WELCOME = 'Witaj ',
  YOU_USER = 'Użytkowniku',
  NOT_REGISTERED = 'Nie jesteś zarejestrowany w naszym systemie',
  REGISTERED_INFORMATION = 'Głównym założeniem aplikacji jest bezpieczeństwo danych. Twoje dane znajdują się tylko na twoim urządzeniu. Możliwość rejestracji w naszym serwerze oznacza możliwość utworzenia kopii zapasowych haseł w chmurze, na wypadek formatu urządzenia lub aplikacji. Nasze serwery są w pełni zabezpieczone. Zapisane hasła nie będą nigdzie upubliczione',
  REGISTERED_ON_SERVER = 'Zarejestrowany na serwerze',
  ENHANCED_PROTECTION = 'Wzmocnione bezpieczeństwo',
}
