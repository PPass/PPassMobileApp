import { AccountResponsesInterface } from '../types/accountResponsesInterface';
import { AccountResponses } from './accountResponses';
import { AccountResponsesPl } from './accountReponsesPL';

export const accountResponses = (userLanguage: string): AccountResponsesInterface => {
  switch (userLanguage) {
    case 'pl_PL':
      return AccountResponsesPl;
    case 'en_US':
      return AccountResponses;
    default:
      return AccountResponses;
  }
};
