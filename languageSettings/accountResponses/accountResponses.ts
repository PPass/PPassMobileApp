export enum AccountResponses {
  WELCOME = 'Welcome ',
  YOU_USER = 'User',
  NOT_REGISTERED = 'You are not registered on our data server',
  REGISTERED_INFORMATION = "This app is built with privacy in mind. It will not send any data anywhere unless you want it to. If you need your passwords backed somewhere, you can register to our data server. That will give you backup of your data in case you'll format your device or move to newer phone. Your passwords will be kept in secured database and not shared anywhere",
  REGISTERED_ON_SERVER = 'Registered on server',
  ENHANCED_PROTECTION = 'Enhanced protection',
}
