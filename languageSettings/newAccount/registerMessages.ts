export enum RegisterMessages {
  REGISTER = 'Register',
  LOGIN = 'Log in',
  PASSWORD = 'Password',
  CONFIRM = 'Confirm',
  CONFIRM_MESSAGE = 'Confirm password to unlock',
  ADD_PASSWORD = 'Add new password',
  SERVICE_NAME = 'Service name',
  ADD = 'Add',
  EDIT = 'Edit',
  PRE_REGISTER_HEADER = 'It looks that you are new here. Click arrow to start or press question mark to read what this app can do',
  WELCOME = 'Welcome',
  HELP_SCREEN = 'This app is simple password manager. It can keep your passwords in encrypted vault and let you copy them whenever required',
}
