import { RegisterMessagesInterface } from '../types/registerMessagesInterface';
import { RegisterMessages } from './registerMessages';
import { RegisterMessagesPL } from './registerMessagesPL';

export const registerMessages = (userLanguage: string): RegisterMessagesInterface => {
  switch (userLanguage) {
    case 'pl_PL':
      return RegisterMessagesPL;
    case 'en_US':
      return RegisterMessages;
    default:
      return RegisterMessages;
  }
};
