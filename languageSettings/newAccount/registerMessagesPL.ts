export enum RegisterMessagesPL {
  REGISTER = 'Zarejestruj się',
  LOGIN = 'Zaloguj',
  PASSWORD = 'Hasło',
  CONFIRM = 'Potwierdź',
  CONFIRM_MESSAGE = 'Potwierdź hasło by kontynuować',
  ADD_PASSWORD = 'Dodaj nowe hasło',
  SERVICE_NAME = 'Nazwa serwisu',
  ADD = 'Dodaj',
  EDIT = 'Edytuj',
  PRE_REGISTER_HEADER = 'Wygląda na to, że jesteś tu nowy. Kliknij w strzałkę by kontynuować, lub naciśnij znak zapytania aby sprawdzić co aplikacja potrafi',
  WELCOME = 'Witaj',
  HELP_SCREEN = 'Aplikacja, którą masz przed sobą, jest prostym menagerem haseł. Jej zadaniem jest bezpieczne trzymanie twoich danych w zaszyfrowanym sejfie',
}
