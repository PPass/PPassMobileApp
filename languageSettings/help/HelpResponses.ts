export enum HelpResponses {
  WHY_NOT_CONNECTED = 'Why am I not registered on server?',
  WHY_NOT_CONNECTED_ANSWER = 'Main goal of this app is to provide as secured and privacy focused experience as possible. ' +
    'That means, it will not share your data anywhere. ' +
    'Being "connected" to our server is optional and provide secured backup of your data in the cloud ( which is accessible from any devices ). ' +
    'For now its still work in progress and its impossible to connect. We are working on it',
  WHY_NOT_ENHANCED_PROTECTION = "Why don't I have enhanced protection? ",
  WHY_NOT_ENHANCED_PROTECTION_ANSWER = 'By default, every password is kept inside of this app as it was written by you. ' +
    'Enhanced protection, means that this password will be changed to random string by encryption. ' +
    'Passwords without additional encryption, are help inside of encrypted database. ' +
    'Its hard to steal them but additional encryption gives you additional security layer. ',
  WHY_UNDER_CONSTRUCTION = 'Why some parts of this app are "under construction" ?',
  WHY_UNDER_CONSTRUCTION_ANSWER = 'I am working on this app alone after work. That means it will take a while to completely finish this app but I ' +
    'will finish this project at least to the point, its fully usable.',
  SECURED = 'How can I know that this app is secured enough ?',
  SECURED_ANSWER = 'Android by default has special isolated location for apps data. This location cannot be accessed from outside ' +
    "without software modifications. That means, its almost impossible to just 'grab' your passwords. I went step " +
    "further. I've device to use 'secured storage' provided by language I am using. This type od storage is " +
    'completely encrypted.',
}
