export enum HelpResponsesPL {
  WHY_NOT_CONNECTED = 'Dlaczego nie jestem połączony z serverem?',
  WHY_NOT_CONNECTED_ANSWER = 'Głównym założeniem aplikacji jest prywatność. Aplikacja ta jest pisana z naciskiem na bezpieczeństwo twoich danych,' +
    ' przez co są one przechowywane wyłączenie na twoim urządzeniu. Możliwosć połączenia z naszymi serverami, daje dodatkową kopię zapasową danych' +
    ' na wypadek formatu urządzenia lub jego uszkodzenia. Dane na naszych serverach są szfryowane, przez co nikt nie będzie miał do nich dostępu, ani nie będzie ich udostępniać. ' +
    'Aktualnie rejestracja w naszym servisie jest w trakcie budowy',
  WHY_UNDER_CONSTRUCTION = 'Dlaczego część aplikacji ma oznakowanie "W trakcie budowy" ?',
  WHY_UNDER_CONSTRUCTION_ANSWER = 'Pracuję nad tą aplikacją samodzielnie. Zbudowanie wszystkich funkcjonalności zajmie trochę czasu.',
  WHY_NOT_ENHANCED_PROTECTION = 'Dlaczego nie mam wzmocnionego bezpieczeństwa?',
  WHY_NOT_ENHANCED_PROTECTION_ANSWER = 'Aplikacja na start, trzyma twoje hasła tak jak je podajesz. Ukrywa je w bezpiecznym miejscu, które jest szyfrowane. ' +
    'Wzmocnione bezpieczeństo oznacza dodatkowo, zaszyfrowanie każdego podawanego hasła co daje silniejsze bezpieczeństwo.',
  SECURED = 'Czy mogę być pewny, że moje hasła są bezpieczne ?',
  SECURED_ANSWER = 'Android natywnie izoluje aplikacji w części systemu, niedostępnej dla zwykłego użytkownika. Każda aplikacja posiada część tej odizolowanej przestrzeni, ' +
    'dzięki czemu nie ma do niej dostępu inna aplikacja czy użytkownik. Oznacza to, iż jest to niemożliwe, by wybrać hasło z tej aplikacji. Dodatkowo, ' +
    "aplikacja ta używa zaszyfrowanej bazy danych o nazwie 'Secured storage'. Dzięki tej bazie, twoje hasła są w 100% zabezpieczone.",
}
