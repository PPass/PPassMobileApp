import { HelpResponses } from './HelpResponses';
import { HelpResponsesPL } from './HelpResponsesPL';
import { HelpMessages } from '../types/helpMessagesInterface';

export const helpMessages = (userLanguage: string): HelpMessages => {
  switch (userLanguage) {
    case 'pl_PL':
      return HelpResponsesPL;
    case 'en_US':
      return HelpResponses;
    default:
      return HelpResponses;
  }
};
